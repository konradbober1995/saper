#include "game.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

Saper::Saper()
{
//curs_set(0);
srand(time(NULL));
short i,j;
win=0;
smin=0;

game = new char *[10];
    for(i=0;i<10;i++)
        {
        game[i] = new char[10];
        }

view = new char *[10];
    for(i=0;i<10;i++)
        {
        view[i] = new char[10];
        }

colour = new short *[10];
    for(i=0;i<10;i++)
        {
        colour[i] = new short[10];
        }

for(i=0;i<10;i++)
    for(j=0;j<10;j++)
        {
        game[j][i]=' ';
        }

for(i=0;i<10;i++)
    for(j=0;j<10;j++)
        {
        view[j][i]=254;
        }

for(i=0;i<10;i++)
    for(j=0;j<10;j++)
        {
        colour[j][i]=0;
        }

}

void Saper::rysuj_ramke()
{
short i;
for(i=1; i<11; i++){
    mvprintw(0,i,"%c",205);
    mvprintw(11,i,"%c",205);
    }
for(i=1; i<11; i++){
    mvprintw(i,0,"%c",186);
    mvprintw(i,11,"%c",186);
    }
mvprintw(0,0,"%c",201);
mvprintw(11,0,"%c",200);
mvprintw(0,11,"%c",187);
mvprintw(11,11,"%c",188);


}


void Saper::wypisz()
{
short i,j;

for(i=0;i<10;i++)
{
    for(j=0;j<10;j++)
    {
    attron(COLOR_PAIR(colour[j][i]));
    mvprintw(j+1,i+1,"%c",view[j][i]);
    attroff(COLOR_PAIR(colour[j][i]));
    //mvprintw(j+1,(i*3)+12,"%d",colour[j][i]);
    //mvprintw(j+1,i+42,"%c",game[j][i]);
    }
}
}

void Saper::losuj()
{
short x,y,i,j,n,mbegx,mbegy,mendx,mendy,mx,my;
for(i=0;i<10;i++)
    {
        do
        {
        x= (int)(rand() / (RAND_MAX + 1.0) * 10);
        y= (int)(rand() / (RAND_MAX + 1.0) * 10);
        }
        while(game[y][x]!=' ');
    game[y][x]='*';
    }

for(j=0;j<10;j++)
for(i=0;i<10;i++)
    {
    n=0;
    if(i==0)
        mbegx=i;
    else
        mbegx=i-1;

    if(j==0)
        mbegy=j;
    else
        mbegy=j-1;

    if(i<9)
        mendx=i+1;
    else
        mendx=i;

    if(j<9)
        mendy=j+1;
    else
        mendy=j;

    for(my=mbegy;my<=mendy;my++)
    for(mx=mbegx;mx<=mendx;mx++)
    {
    if(mx==i&&my==j)
        continue;
    if(game[my][mx]=='*')
        n++;
    }


    if(n>0&&game[j][i]==' ')
        game[j][i]=n+48;

    }

}

void Saper::petla_gry()
{
MEVENT event_mouse;
mousemask(ALL_MOUSE_EVENTS, NULL);
char znak;
short m_x,m_y,x,y,lastx=1,lasty=1;
noecho();
nodelay(stdscr,1);
curs_set(0);
keypad(stdscr,true);
rysuj_ramke();
losuj();

start_color();
init_pair( 0, COLOR_WHITE, COLOR_BLACK );
init_pair( 1, COLOR_RED, COLOR_BLACK );
init_pair( 2, COLOR_GREEN, COLOR_BLACK );
init_pair( 3, COLOR_BLUE, COLOR_BLACK );
init_pair( 4, COLOR_YELLOW, COLOR_BLACK );

init_pair( 10, COLOR_BLACK, COLOR_WHITE );
init_pair( 11, COLOR_RED, COLOR_WHITE );
init_pair( 12, COLOR_GREEN, COLOR_WHITE );
init_pair( 13, COLOR_BLUE, COLOR_WHITE );
init_pair( 14, COLOR_YELLOW, COLOR_WHITE );

colour[1][1]=10;
do
{
    znak=getch();
    nc_getmouse(&event_mouse);

    wypisz();
    m_x=event_mouse.x;
    m_y=event_mouse.y;
//mvprintw(14,2,"(%d,%d), win:%d smin:%d",m_y,m_x,win,smin);

    if(lastx!=(m_x-1)||lasty!=(m_y-1))
    {
        if((m_x>0&&m_x<11)&&(m_y>0&&m_y<11))
        {
        colour[lasty][lastx]-=10;
        lastx=m_x-1;
        lasty=m_y-1;
        colour[lasty][lastx]+=10;
        }
    }


    if(event_mouse.bstate&BUTTON1_CLICKED)
    {
        x=m_x-1;
        y=m_y-1;
        if((m_x>0&&m_x<11)&&(m_y>0&&m_y<11))
        {
            if(view[y][x]==-2)
            {
                if(game[y][x]=='*')
                    {
                    mvprintw(m_y,m_x,"*");
                    mvprintw(15,1,"Koniec gry");
                    nodelay(stdscr,0);
                    getch();
                    break;
                    }
                else
                    if(game[y][x]==' ')
                    {
                    mvprintw(m_y,m_x," ");
                    view[y][x]=' ';
                    game[y][x]='x';
                    win++;
                    nodelay(stdscr,0);
                    puste(y,x);
                    nodelay(stdscr,1);
                    }
                    else
                    if(game[y][x]=='x');
                        else
                        {
                        mvprintw(m_y,m_x,"%c",game[y][x]);
                        view[y][x]=game[y][x];
                        colour[y][x]=game[y][x]-38;
                        win++;
                        }
            }
            if(win>89){
            mvprintw(15,1,"Wygra\210es");
            nodelay(stdscr,0);
            getch();
            break;
            }

        }
    }
    if(event_mouse.bstate&BUTTON3_CLICKED)
            {
            m_x=event_mouse.x;
            m_y=event_mouse.y;
            if(view[m_y-1][m_x-1]==-2)
                view[m_y-1][m_x-1]='X';
            else
            if(view[m_y-1][m_x-1]=='X')
                view[m_y-1][m_x-1]=254;
            }
}
while(1);

}


void Saper::puste(short y,short x)
{

if(x<9){
if(game[y][x+1]==' ')
    {
    view[y][x+1]=' ';
    game[y][x+1]='x';
    colour[y][x+1]=0;
    win++;
    puste(y,x+1);
    }
else{
    if(game[y][x+1]<57&&colour[y][x+1]==0){
    view[y][x+1]=game[y][x+1];
    colour[y][x+1]=game[y][x+1]-48;
    win++;
    smin++;
    }
     if(y<9){
        if(game[y+1][x+1]<57&&game[y+1][x+1]!=' '&&colour[y+1][x+1]==0)
            {
            view[y+1][x+1]=game[y+1][x+1];
            colour[y+1][x+1]=game[y+1][x+1]-48;
            //mvprintw(17,1,"(%d,%d), yx:(%d,%d)",y+1,x+1,y,x);
            //getch();
            //smin++;
            win++;
            }
        }
    }
}

if(x>0){
if(game[y][x-1]==' ')
    {
    view[y][x-1]=' ';
    game[y][x-1]='x';
    colour[y][x-1]=0;
    win++;
    puste(y,x-1);
    }
else{
    if(game[y][x-1]<57&&colour[y][x-1]==0){
    view[y][x-1]=game[y][x-1];
    colour[y][x-1]=game[y][x-1]-48;
    win++;
    //smin++;
    }
      if(y>0){
        if(game[y-1][x-1]<57&&game[y-1][x-1]!=' '&&colour[y-1][x-1]==0)
            {
            view[y-1][x-1]=game[y-1][x-1];
            colour[y-1][x-1]=game[y-1][x-1]-48;
            //mvprintw(17,1,"(%d,%d), yx:(%d,%d)",y-1,x-1,y,x);
            //getch();
            //smin++;
            win++;
            }
        }
    }
}

if(y<9){
if(game[y+1][x]==' ')
    {
    view[y+1][x]=' ';
    game[y+1][x]='x';
    colour[y+1][x]=0;
    win++;
    puste(y+1,x);
    }
else{
    if(game[y+1][x]<57&&colour[y+1][x]==0){
    view[y+1][x]=game[y+1][x];
    colour[y+1][x]=game[y+1][x]-48;
    win++;
    //smin++;
    }
       if(x>0){
        if(game[y+1][x-1]<57&&game[y+1][x-1]!=' '&&colour[y+1][x-1]==0)
            {
            view[y+1][x-1]=game[y+1][x-1];
            colour[y+1][x-1]=game[y+1][x-1]-48;
            //smin++;
            win++;
            }
        }
    }
}

if(y>0){
if(game[y-1][x]==' ')
    {
    view[y-1][x]=' ';
    game[y-1][x]='x';
    colour[y-1][x]=0;
    win++;
    puste(y-1,x);
    }
else{
    if(game[y-1][x]<57&&colour[y-1][x]==0){
    view[y-1][x]=game[y-1][x];
    colour[y-1][x]=game[y-1][x]-48;
    win++;
    //smin++;
    }
        if(x<9){
        if(game[y-1][x+1]<57&&game[y-1][x+1]!=' '&&colour[y-1][x+1]==0)
            {
            view[y-1][x+1]=game[y-1][x+1];
            colour[y-1][x+1]=game[y-1][x+1]-48;
            //mvprintw(17,1,"(%d,%d), yx:(%d,%d)",y-1,x+1,y,x);
            //getch();
            //smin++;
            win++;
            }
        }
    }
}

}
